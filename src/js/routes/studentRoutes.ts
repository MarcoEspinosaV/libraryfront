import {IRouteItem} from "./index";
import {AUTHENTICATION_CONSTANTS} from "../commons/constants/authenticationConstants";
import {THEMES_CONSTANTS} from "../commons/constants/themesConstants";
import HomeContainer from "../containers/views/HomeContainer";

export const STUDENT_ROUTES: { [name: string]: IRouteItem } = {
    STUDENT_HOME: {
        authentication: AUTHENTICATION_CONSTANTS.TOKEN,
        component: HomeContainer,
        hideSideNav: true,
        id: 'student_home',
        key: 'student home',
        name: {
            es: 'Inicio',
            en: 'Home'
        },
        path: '/home',
        roles: [],
        showOn: [AUTHENTICATION_CONSTANTS.TOKEN],
        showTitles: false,
        theme: THEMES_CONSTANTS.TOKEN,
        title: {
            es: 'Inicio',
            en: 'Home'
        }
    }
};