import {IRouteItem} from "./index";
import {AUTHENTICATION_CONSTANTS} from "../commons/constants/authenticationConstants";
import {THEMES_CONSTANTS} from "../commons/constants/themesConstants";
import LoginContainer from "../containers/views/LoginContainer";

export const LOGIN_ROUTES: { [name: string]: IRouteItem } = {
    LOGIN: {
        authentication: AUTHENTICATION_CONSTANTS.LOGIN,
        component: LoginContainer,
        hideSideNav: true,
        id: 'login',
        key: 'login',
        name: {
            es: 'Ingreso',
            en: 'Login'
        },
        path: '/',
        roles: [],
        showOn: [AUTHENTICATION_CONSTANTS.LOGIN],
        showTitles: false,
        theme: THEMES_CONSTANTS.LOGIN,
        title: {
            es: 'Ingreso',
            en: 'Login'
        }
    }
};