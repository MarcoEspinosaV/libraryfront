import {LOGIN_ROUTES} from "./loginRoutes";
import {STUDENT_ROUTES} from "./studentRoutes";

export interface IRouteItem {
  authentication: string;
  component: object | null;
  icon?: any;
  id: string;
  hideSideNav?: boolean;
  key: string;
  name: {
    es: string;
    en: string;
  };
  path?: string;
  redirectPath?: string;
  pathParams?: string;
  parentId?: string;
  roles: string[];
  showOn: string[];
  showTitles: boolean;
  subTitle?: string;
  theme: string;
  title: {
    es: string;
    en: string;
  };
  [name: string]: any;
}

export const ROUTES: { [name: string]: any } = {
  ...LOGIN_ROUTES,
  ...STUDENT_ROUTES
};

export const appConfigRoutes = () => {
  const appRoutes: Array<{ path: string; component: any; pathParams?: string }> = [];
  Object.keys(ROUTES).forEach(route => {
    const routeData = ROUTES[route];
    if (routeData.path) {
      appRoutes.push({ path: routeData.path, component: routeData.component, pathParams: routeData.pathParams });
    }
  });
  return appRoutes;
};
