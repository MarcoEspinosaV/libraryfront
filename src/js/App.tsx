import * as React from 'react';
import { Provider } from 'react-redux';

import {appConfigRoutes} from "./routes";
import configStore, { history } from './configurations/configStore';
import { Route, Switch } from 'react-router-dom';
import {ConnectedRouter} from "connected-react-router";
import {PersistGate} from "redux-persist/integration/react";

const { store, persistor } = configStore();

class App extends React.Component {
  public render() {
    const appRoutes = appConfigRoutes();
    const renderRoute = (routeData: any) => (
        <div className="main-div">
          <Switch location={routeData.location}>
            {appRoutes.map(route => (
                <Route
                    key={route.path}
                    path={`${route.path}${route.pathParams ? '/' + route.pathParams : ''}`}
                    exact={true}
                    component={route.component}
                />
            ))}
          </Switch>
        </div>
    );
    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <ConnectedRouter history={history}>
                    <Route render={renderRoute} />
                </ConnectedRouter>
            </PersistGate>
        </Provider>
    );
  }
}

export default App;
