import * as React from 'react';

type IValue = string | number
type IType = 'text' | 'number' | 'password'
type IStyle = 'FULL';

interface IProps {
    label?: string;
    value: IValue;
    id: string;
    type: IType
    onChange: (data: any, key: string) => void;
    style?: IStyle;
}

interface IState {
    focusClass: string;
}

export default class InputGenericComponent extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            focusClass: ''
        }
    }

    handleFocus = () => {
        this.setState({ focusClass: 'input-focus' })
    };

    handleBlur = () => {
        this.setState( { focusClass: '' })
    };

    render() {
        const { label, id, type, onChange, value, style } = this.props;
        const { focusClass } = this.state;
        const inputClass = [
            'input-generic-component',
            focusClass,
            style ? 'FULL' : ''
        ].join(' ');
        return (
            <div className={inputClass} onFocus={() => this.handleFocus()} onBlur={() => this.handleBlur()}>
                {label && (<label htmlFor={id} className="input-label">
                    {label}
                </label>)}
                <div id={id}>
                    <input type={type} spellCheck={false} onChange={e => onChange(e.target.value, id)} value={value} />
                </div>
            </div>
        );
    }
}
