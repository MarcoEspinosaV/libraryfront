import * as React from 'react';

interface IData {
    title: string;
    image: string;
    status: string;
    author: string;
    category: string;
    editorial: string;
}

interface IProps {
    data: IData;
}

export default class CardGenericComponent extends React.Component<IProps>{
    render() {
        const {data} = this.props;
        return (
            <div className="card-generic-component">
                <div className="category">
                    {data.category}
                </div>
                <div className="title">
                    {data.title}
                </div>
                <img src={data.image} alt="book"/>
                <div className="author">
                    {data.author}
                </div>
                <div className="editorial">
                    {data.editorial}
                </div>
            </div>
        );
    }
}