import * as React from 'react';
import {IMAGES} from "../../commons/constants/images";

interface IProps {
    onClick: (state: any) => void;
    stateModal: boolean;
}

export default class ModalGenericComponent extends React.Component<IProps>{
    render() {
        const {children, onClick, stateModal} = this.props;
        return (
            <div className={`modal-generic-component ${stateModal ? 'active' : 'no-active'}`}>
                <div className="modal-content">
                    <div className="close-btn" onClick={() => onClick(!stateModal)}>
                        <img src={IMAGES.CLOSE} alt=""/>
                    </div>
                    {children}
                </div>
            </div>
        );
    }
}