import * as React from 'react';

import {IMAGES} from "../../commons/constants/images";

interface IData {
    avatar?: string;
    name: string;
    lastName: string;
}

interface IProps {
    data: IData;
}

export default class SelectorGenericComponent extends React.Component<IProps> {
    render() {
        const {data} = this.props;
        return (
            <div className="selector-generic-component">
                <div className="selector-main-image" style={{backgroundImage: `url(${IMAGES.AVATAR})`}} />
                <div className="selector-dropdown">
                    <div className="selector-dropdown-main-label">
                        {data.name + ' ' + data.lastName}
                    </div>
                    <img src={IMAGES.ARROW_DOWN} alt="selector-arrow-down" className="selector-dropdown-arrow-down"/>
                </div>
            </div>
        );
    }
}
