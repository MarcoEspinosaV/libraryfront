import * as React from 'react';

import {IHomeContainerProps} from "../../containers/views/HomeContainer";
import SelectorGenericComponent from "../generics/SelectorGenericComponent";
import CardGenericComponent from "../generics/cardGenericComponent";
import {IMAGES} from "../../commons/constants/images";
import ModalGenericComponent from "../generics/modalGenericComponent";
import InputGenericComponent from "../generics/InputGenericComponent";

interface IState {
    isOpen: boolean;
}

export default class HomeView extends React.Component<IHomeContainerProps, IState> {
    constructor(props: IHomeContainerProps) {
        super(props);
        this.state = {
            isOpen: false
        }
    }

    onClick = () => {
        this.setState({isOpen: !this.state.isOpen})
    };
    render() {
        const {studentData} = this.props;
        const booksData = [
            {
                title: 'Programacion funcional',
                image: IMAGES.BOOK,
                status: 'Disponible',
                category: 'Software',
                author: 'Alvin Alexander',
                editorial: 'O`REILLY'
            },
            {
                title: 'Programacion Orientada a Objetos',
                image: IMAGES.BOOK,
                status: 'Disponible',
                category: 'Software',
                author: 'Alvin Alexander',
                editorial: 'O`REILLY'
            },
            {
                title: 'Base de datos no relacional',
                image: IMAGES.BOOK,
                status: 'Disponible',
                category: 'Software',
                author: 'Alvin Alexander',
                editorial: 'O`REILLY'
            }
        ];
        return (
            <div className="home-view">
                <ModalGenericComponent onClick={this.onClick} stateModal={this.state.isOpen}>
                    <div className="modal-content-home">
                        <div className="tittle">
                            Solicitud de libro
                        </div>
                        <div className="sub-title">
                            Llena los siguientes campos del formulario para completar tu solicitud
                        </div>
                        <div className="personal-data">
                            Datos del estudiante
                        </div>
                        <div className="input-container">
                            <div className="input-content">
                                <InputGenericComponent value={'Marco Espinosa'} id={'name'} type={'text'} onChange={() => null}
                                                       label={'Nombres del Estudiante'} />
                            </div>
                            <div className="input-content">
                                <InputGenericComponent value={'Ing. Software'} id={'carrera'} type={'text'} onChange={() => null}
                                                       label={'Carrera que cursas'} />
                            </div>
                            <div className="input-content">
                                <InputGenericComponent value={'Ing. Software'} id={'carrera'} type={'text'} onChange={() => null}
                                                       label={'Carrera que cursas'} />
                            </div>
                        </div>
                        <div className="personal-data">
                            Datos del libro
                        </div>
                        <div className="input-container">
                            <div className="input-content">
                                <InputGenericComponent value={'Programacion funcional'} id={'name'} type={'text'} onChange={() => null}
                                                       label={'Titulo'} />
                            </div>
                            <div className="input-content">
                                <InputGenericComponent value={'Alvin Alexander'} id={'carrera'} type={'text'} onChange={() => null}
                                                       label={'Autor'} />
                            </div>
                            <div className="input-content">
                                <InputGenericComponent value={'O`REILLY'} id={'carrera'} type={'text'} onChange={() => null}
                                                       label={'Editorial'} />
                            </div>
                            <div className="input-content">
                                <InputGenericComponent value={'Programacion - Software'} id={'carrera'} type={'text'} onChange={() => null}
                                                       label={'Categoria'} />
                            </div>
                            <div className="input-content">
                                <InputGenericComponent value={'CDA-0001'} id={'carrera'} type={'text'} onChange={() => null}
                                                       label={'Ubicacion fisica del libro'} />
                            </div>
                        </div>
                        <div className="personal-data">
                            Datos del pedido
                        </div>
                        <div className="input-container">
                            <div className="input-content">
                                <InputGenericComponent value={'28/06/2019'} id={'initialDate'} type={'text'} onChange={() => null}
                                                       label={'Fecha de inicio'} />
                            </div>
                            <div className="input-content">
                                <InputGenericComponent value={'04/06/2019'} id={'endDate'} type={'text'} onChange={() => null}
                                                       label={'Fecha de fin'} />
                            </div>
                        </div>
                        <div className="btn-accepted">
                            <div>
                                Solicitar
                            </div>
                        </div>
                    </div>
                </ModalGenericComponent>
                <div className="home-sidenav">
                    <div>
                        <div className="icon-content">
                            <img src={IMAGES.DASHBOARD} alt="dashboard" className="icon-sidenav"/>
                        </div>
                        <div className="icon-content">
                            <img src={IMAGES.OPEN_BOOK} alt="open book" className="icon-sidenav"/>
                        </div>
                        <div className="icon-content">
                            <img src={IMAGES.SHOPPING_CART} alt="shopping cart" className="icon-sidenav"/>
                        </div>
                    </div>
                    <div>
                        <div className="icon-content">
                            <img src={IMAGES.SETTINGS} alt="settings" className="icon-sidenav"/>
                        </div>
                        <div className="icon-content">
                            <img src={IMAGES.BELL} alt="bell" className="icon-sidenav"/>
                        </div>
                        <div className="icon-content">
                            <img src={IMAGES.USER} alt="user" className="icon-sidenav"/>
                        </div>
                    </div>
                </div>
                <div className="home-header">
                    <SelectorGenericComponent data={studentData}/>
                </div>
                <div className="home-content">
                    <div className="books-content">
                        <div className="book-content-title">
                            Todos los libros
                        </div>
                        <div className="book-content-data">
                            {booksData.map((item, key) => (
                                <div key={key} className="card-container" onClick={this.onClick}>
                                    <CardGenericComponent data={item} />
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
