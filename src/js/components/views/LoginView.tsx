import * as React from 'react';

import {ILoginContainerProps} from "../../containers/views/LoginContainer";
import {IMAGES} from "../../commons/constants/images";
import InputGenericComponent from "../generics/InputGenericComponent";
import {ROUTES} from "../../routes";

export default class LoginView extends React.Component<ILoginContainerProps> {
    render() {
        const { labels, onChangeInput, loginData } = this.props;
        return (
            <div className="login-view">
                <img src={IMAGES.LIBRARY_LOGIN} className="login-main-image" alt="login-main"/>
                <div className="login-form">
                    <div className="login-form-main-title">
                        {labels.WELCOME_LIBRARY}
                    </div>
                    <div className="login-form-sub-label">
                        {labels.ENTRY_WITH_YOUR_ACCOUNT}
                    </div>
                    <div className="login-form-input">
                        <InputGenericComponent id={'email'} value={loginData.email} label={labels.EMAIL} type={"text"} onChange={onChangeInput} />
                    </div>
                    <div className="login-form-input">
                        <InputGenericComponent id={'password'} value={loginData.password} label={labels.PASSWORD} type={"password"} onChange={onChangeInput} />
                    </div>
                    <div className="login-form-footer">
                        <a href="/" className="login-form-footer-link">{labels.FORGOT_PASSWORD}</a>
                        <a className="login-form-footer-button" href={ROUTES.STUDENT_HOME.path}>
                            {labels.LOGIN}
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}
