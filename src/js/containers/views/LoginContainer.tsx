import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {AnyAction} from 'redux';

import LoginView from "../../components/views/LoginView";
import {IApp} from "../../reducers/rootReducer";
import {ILoginLanguage} from "../../languages/en/loginLabels";
import {getLabels} from "../../languages";
import {ILoginReducerData, loginState} from "../../reducers/loginReducer";
import {LOGIN_ACTIONS} from "../../actions/indexActions";

interface IMapStateToProps {
    labels: ILoginLanguage;
    loginData: ILoginReducerData
}

interface IMapDispatchToProps {
  onChangeInput: (data: string, key: string) => void;
}

export type ILoginContainerProps = IMapStateToProps & IMapDispatchToProps;

const mapStateToProps = (state: IApp): IMapStateToProps => {
  const { loginData } = state.loginReducer || loginState;
  return {
      labels: getLabels('loginLabels', 'es'),
      loginData: loginData
  };
};

const mapDispatchToProps = (dispatch: (data: AnyAction) => void): IMapDispatchToProps => {
  return {
    onChangeInput: (data, key) => {
      dispatch({
        payload: {data, key},
        type: LOGIN_ACTIONS.LOGIN_ACTIONS_ON_CHANGE_INPUT
      })
    }
  };
};

function mergeProps(
  stateProps: IMapStateToProps,
  dispatchProps: IMapDispatchToProps
): ILoginContainerProps {
  return {
    ...stateProps,
    ...dispatchProps
  };
}

export default withRouter(
  connect<IMapStateToProps, IMapDispatchToProps, any>(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(LoginView)
);
