import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {AnyAction} from 'redux';
import {IApp} from "../../reducers/rootReducer";
import HomeView from "../../components/views/HomeView";
import {homeState, IStudentData} from "../../reducers/homeReducer";

interface IMapStateToProps {
    studentData: IStudentData;
}

interface IMapDispatchToProps {

}

export type IHomeContainerProps = IMapStateToProps & IMapDispatchToProps;

const mapStateToProps = (state: IApp): IMapStateToProps => {
    const {studentData} = state.homeReducer || homeState;
    return {
      studentData: studentData
  };
};

const mapDispatchToProps = (dispatch: (data: AnyAction) => void): IMapDispatchToProps => {
  return {

  };
};

function mergeProps(
  stateProps: IMapStateToProps,
  dispatchProps: IMapDispatchToProps
): IHomeContainerProps {
  return {
    ...stateProps,
    ...dispatchProps
  };
}

export default withRouter(
  connect<IMapStateToProps, IMapDispatchToProps, any>(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(HomeView)
);
