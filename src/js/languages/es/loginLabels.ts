import {ILoginLanguage} from "../en/loginLabels";

const loginLabels: ILoginLanguage = {
    WELCOME_LIBRARY: '¡Bienvenid@ a Biblioteca!',
    ENTRY_WITH_YOUR_ACCOUNT: 'Ingresa con tu cuenta universitaria',
    EMAIL: 'Correo electrónico',
    PASSWORD: 'Contraseña',
    FORGOT_PASSWORD: '¿Olvidaste tu contraseña?',
    LOGIN: 'Ingresar'
};

export default loginLabels;