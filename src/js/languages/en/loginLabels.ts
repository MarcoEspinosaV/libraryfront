export interface ILoginLanguage {
    WELCOME_LIBRARY: string;
    ENTRY_WITH_YOUR_ACCOUNT: string;
    EMAIL: string;
    PASSWORD: string;
    FORGOT_PASSWORD: string;
    LOGIN: string;
}

const loginLabels: ILoginLanguage = {
    WELCOME_LIBRARY: 'Welcome to Biblioteca!',
    ENTRY_WITH_YOUR_ACCOUNT: 'Entry with your university account',
    EMAIL: 'Email',
    PASSWORD: 'Password',
    FORGOT_PASSWORD: 'Forgot your password?',
    LOGIN: 'Login'
};

export default loginLabels;