export const APP_LANGUAGES = {
  en_US: {
    id: 'en',
    label: 'English'
  },
  es_EC: {
    id: 'es',
    label: 'Español'
  }
};

export const getLabels = (labelsFileName: string, language?: string) => {
  switch (language) {
    case APP_LANGUAGES.en_US.id:
      return require(`./en/${labelsFileName}`).default;
    default:
      return require(`./es/${labelsFileName}`).default;
  }
};
