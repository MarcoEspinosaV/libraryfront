import {AnyAction} from "redux";
import {LOGIN_ACTIONS} from "../actions/indexActions";

export interface ILoginReducerData {
    email: string;
    password: string;
}

export interface ILoginReducer {
    loginData: ILoginReducerData;
}

export const loginState: ILoginReducer = {
    loginData: {
        email: '',
        password: ''
    }
};

const loginReducer = (state: ILoginReducer = loginState, action: AnyAction): ILoginReducer => {
    const loginData: any = { ...state.loginData };
    switch (action.type) {
        case LOGIN_ACTIONS.LOGIN_ACTIONS_ON_CHANGE_INPUT:
            loginData[action.payload.key] = action.payload.studentData;
            return {
                ...state,
                loginData
            };
        default:
            return {
                ...state
            }
    }
};

export default loginReducer;