import { connectRouter, RouterState } from 'connected-react-router';
import { History } from 'history';
import {combineReducers} from 'redux';
import loginReducer, {ILoginReducer} from "./loginReducer";
import homeReducer, {IHomeReducer} from "./homeReducer";

export interface IApp {
    router?: RouterState;
    loginReducer?: ILoginReducer;
    homeReducer?: IHomeReducer;
}

export default (history: History): any =>
    combineReducers({
        router: connectRouter(history),
        loginReducer,
        homeReducer
    });