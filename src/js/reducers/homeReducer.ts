import {AnyAction} from "redux";

export interface IStudentData {
    avatar?: string;
    name: string;
    lastName: string;
}

export interface IHomeReducer {
    studentData: IStudentData;
}

export const homeState: IHomeReducer = {
    studentData: {
        name: 'Marco',
        lastName: 'Espinosa'
    }
};

const homeReducer = (state: IHomeReducer = homeState, action: AnyAction): IHomeReducer => {
    const studentData: any = { ...state.studentData };
    switch (action.type) {
        default:
            return {
                ...state
            }
    }
};

export default homeReducer;