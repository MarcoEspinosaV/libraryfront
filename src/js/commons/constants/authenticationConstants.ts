interface IAuthenticationConstants {
    LOGIN: 'LOGIN',
    PUBLIC: 'PUBLIC',
    TOKEN: 'TOKEN'
}

export const AUTHENTICATION_CONSTANTS: IAuthenticationConstants = {
    LOGIN: 'LOGIN',
    PUBLIC: 'PUBLIC',
    TOKEN: 'TOKEN'
};
