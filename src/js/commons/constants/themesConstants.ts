interface IThemesConstants {
    LOGIN: 'LOGIN',
    PUBLIC: 'PUBLIC',
    TOKEN: 'TOKEN'
}

export const THEMES_CONSTANTS: IThemesConstants = {
    LOGIN: 'LOGIN',
    PUBLIC: 'PUBLIC',
    TOKEN: 'TOKEN'
};
