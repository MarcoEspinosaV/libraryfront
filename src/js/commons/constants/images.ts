interface IImagesConstants {
    LIBRARY_LOGIN: string;
    AVATAR: string;
    ARROW_DOWN: string;
    BOOK: string;
    DASHBOARD: string;
    OPEN_BOOK: string;
    SETTINGS: string;
    SHOPPING_CART: string;
    USER: string;
    BELL: string;
    CLOSE: string;
}

export const IMAGES: IImagesConstants = {
    LIBRARY_LOGIN: require('../../../images/library_login.jpg'),
    AVATAR: require('../../../images/avatar.svg'),
    ARROW_DOWN: require('../../../images/arrow_down.svg'),
    BOOK: require('../../../images/book.jpg'),
    DASHBOARD: require('../../../images/dashboard.svg'),
    OPEN_BOOK: require('../../../images/open-book.svg'),
    SETTINGS: require('../../../images/settings.svg'),
    SHOPPING_CART: require('../../../images/shoping-cart.svg'),
    USER: require('../../../images/user.svg'),
    BELL: require('../../../images/bell.svg'),
    CLOSE: require('../../../images/close.svg')
};
